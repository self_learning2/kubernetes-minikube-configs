FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY demo-0.0.1-SNAPSHOT.war app.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java -jar /app.jar
